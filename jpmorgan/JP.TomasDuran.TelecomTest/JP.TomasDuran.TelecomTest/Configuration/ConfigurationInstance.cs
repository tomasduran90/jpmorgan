﻿using JP.TomasDuran.TelecomTest.Configuration.Sections;
using JP.TomasDuran.TelecomTest.Configuration.Sections.Plan;
using JP.TomasDuran.TelecomTest.Configuration.Sections.Plugin;
using JP.TomasDuran.TelecomTest.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration
{
    public class ConfigurationInstance
    {
        private static readonly object syncroot = new object();
        private static ConfigurationInstance instance;
        private PlanSection plansconfig;
        private RateCalculatorSection rateCalulator;
        private Dictionary<DayOfWeek, AbstractDayElement> days;
        private Dictionary<string, PlanElement> plans;
        private PlanElement principalPlan;
        private List<LogicalPlugin> plugins;

        private ConfigurationInstance()
        {
            try
            {
                this.plansconfig = (PlanSection)ConfigurationManager.GetSection("PlanSection");
                this.rateCalulator = (RateCalculatorSection)ConfigurationManager.GetSection("RatesCalculatorSection");

                ValidateConfiguration();
            }
            catch (Exception ex)
            {
                throw new ConfigurationErrorsException(ex.Message);
            }
        }

        public static ConfigurationInstance Instance
        {
            get
            {
                lock (syncroot)
                {
                    if (instance == null)
                        instance = new ConfigurationInstance();
                }

                return instance;
            }
        }

        public AbstractDayElement GetDayWorker(DayOfWeek day)
        {
            return this.days[day];
        }

        public PlanElement GetPrincipalPlan
        {
            get
            {
                return this.principalPlan;
            }
        }

        public PlanElement GetPlanByName(string planName)
        {
            return this.plans[planName];
        }

        public List<LogicalPlugin> LogicalPlugins
        {
            get
            {
                return this.plugins;
            }
        }

        //private void LoadPlugins()
        //{
        //    LogicalPluginSection element = (LogicalPluginSection)ConfigurationManager.GetSection("LogicalPluginSection");
        //    plugins = new List<LogicalPlugin>();

        //    foreach (PluginElement pl in element.PluginsCollection)
        //    {
        //        try
        //        {
        //            var pluginInstance = Activator.CreateInstance(Type.GetType(pl.type)) as LogicalPlugin;

        //            if (pluginInstance != null)
        //                plugins.Add(pluginInstance);
        //        }
        //        catch(Exception ex)
        //        {
        //            throw new FormatException("Could not load type: "  + pl.type);
        //        }
        //    }
        //}

        private void ValidateConfiguration()
        {
            this.plans = new Dictionary<string, PlanElement>();
            this.days = new Dictionary<DayOfWeek, AbstractDayElement>();


            //ValidatePlans keys
            foreach (PlanElement plan in this.plansconfig.PlansCollection)
            {
                this.plans.Add(plan.Key, plan);
            }

            //Save principal Plan represents all ranges that no are in config
            if (!this.plans.ContainsKey(this.rateCalulator.RateCalculator.Plan))
            {
                throw new Exception(string.Format("Plan {0} do not exists in configuration.", this.rateCalulator.RateCalculator.Plan));
            }

            this.principalPlan = this.plans[this.rateCalulator.RateCalculator.Plan];


            //Validate Days
            List<PropertyInfo> daysFields = this.rateCalulator.RateCalculator.GetType().GetProperties()
                .Where(t => typeof(AbstractDayElement).IsAssignableFrom(t.PropertyType)).ToList();

            foreach (PropertyInfo day in daysFields)
            {
                var dayInfoProp = day.GetValue(this.rateCalulator.RateCalculator, null) as AbstractDayElement;

                //Validate planExists
                foreach (RangeElement dayRange in dayInfoProp.RangeCollection)
                {
                    if (!this.plans.ContainsKey(dayRange.Plan))
                    {
                        throw new Exception(string.Format("Plan {0} do not exists in configuration.", dayRange.Plan));
                    }
                }

                if (!this.days.ContainsKey(dayInfoProp.Day))
                {
                    //Validate overlapping ranges for same day
                    if (dayInfoProp.RangeCollection != null && dayInfoProp.RangeCollection.Count > 1)
                    {
                        this.ValidateOverlappingRanges(dayInfoProp);
                    }
                    else
                    {
                        //todo intentar entrar por el [0] siempre vale lo mismo
                        foreach (RangeElement item in dayInfoProp.RangeCollection)
                        {
                            if (item.AllDay)
                            {
                                dayInfoProp.Validators.Add(new RangeValidator(item.Plan, "00:00", "23:59") { });
                            }
                            else
                            {
                                dayInfoProp.Validators.Add(new RangeValidator(item.Plan, item.From, item.To) { });
                            }
                        }
                    }

                    this.days.Add(dayInfoProp.Day, dayInfoProp);
                }
                else
                {
                    throw new Exception(string.Format("Day {0} is repeated in configuration.", dayInfoProp.Day));
                }
            }

        }


        private void ValidateOverlappingRanges(AbstractDayElement day)
        {
            foreach (RangeElement range in day.RangeCollection)
            {
                if (range.AllDay == true)
                {
                    throw new Exception("Overlapping Ranges in app.config, same rate can not have AllDay range with others to from values");
                }

                foreach (RangeValidator currentValidator in day.Validators)
                {
                    if (currentValidator.isInRange(range.From) || currentValidator.isInRange(range.To))
                    {
                        throw new Exception("Overlapping Ranges in app.config");
                    }
                }

                RangeValidator validator = new RangeValidator(range.Plan, range.From, range.To);
                day.AddValidator(validator);
            }
        }

    }
}
