﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections
{
    public class SaturdayElement : AbstractDayElement
    {
        public SaturdayElement() : base(DayOfWeek.Saturday) { }
    }
}
