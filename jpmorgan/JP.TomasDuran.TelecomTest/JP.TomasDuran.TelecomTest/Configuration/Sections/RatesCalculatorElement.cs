﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections
{
    public class RatesCalculatorElement : ConfigurationElement
    {
        [ConfigurationProperty("plan", IsRequired = true)]
        public string Plan
        {
            get
            {
                return (string)this["plan"];
            }
            set
            {
                this["plan"] = value;
            }
        }

        [ConfigurationProperty("Monday", IsRequired = false)]
        public MondayElement Monday 
        {
            get
            {
                return (MondayElement)this["Monday"];
            }
            set
            {
                this["Monday"] = value;
            }
        }

        [ConfigurationProperty("Tuesday", IsRequired = false)]
        public TuesdayElement Tuesday
        {
            get
            {
                return (TuesdayElement)this["Tuesday"];
            }
            set
            {
                this["Tuesday"] = value;
            }
        }


        [ConfigurationProperty("Wednesday", IsRequired = false)]
        public WednesdayElement Wednesday
        {
            get
            {
                return (WednesdayElement)this["Wednesday"];
            }
            set
            {
                this["Wednesday"] = value;
            }
        }

        [ConfigurationProperty("Thursday", IsRequired = false)]
        public ThursdayElement Thursday
        {
            get
            {
                return (ThursdayElement)this["Thursday"];
            }
            set
            {
                this["Thursday"] = value;
            }
        }

        [ConfigurationProperty("Friday", IsRequired = false)]
        public FridayElement Friday
        {
            get
            {
                return (FridayElement)this["Friday"];
            }
            set
            {
                this["Friday"] = value;
            }
        }

        [ConfigurationProperty("Saturday", IsRequired = false)]
        public SaturdayElement Saturday
        {
            get
            {
                return (SaturdayElement)this["Saturday"];
            }
            set
            {
                this["Saturday"] = value;
            }
        }

        [ConfigurationProperty("Sunday", IsRequired = false)]
        public SundayElement Sunday
        {
            get
            {
                return (SundayElement)this["Sunday"];
            }
            set
            {
                this["Sunday"] = value;
            }
        }
    }
}
