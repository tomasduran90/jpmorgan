﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections
{
    public class MondayElement : AbstractDayElement
    {
        public MondayElement() : base(DayOfWeek.Monday) { }
    }
}
