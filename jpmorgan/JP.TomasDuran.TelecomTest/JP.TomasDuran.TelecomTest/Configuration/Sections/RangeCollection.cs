﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections
{
    [ConfigurationCollection(typeof(RangeElement), AddItemName = "Range")]
    public class RangeCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new RangeElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null)
                throw new ArgumentNullException("Range");

            return ((RangeElement)element).Plan + ((RangeElement)element).From + ((RangeElement)element).To;
        }
    }
}
