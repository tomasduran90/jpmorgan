﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections.Plugin
{
    class LogicalPluginSection : ConfigurationSection
    {
        [ConfigurationProperty("Plugins", IsDefaultCollection = true)]
        public PluginCollections PluginsCollection
        {
            get
            {
                return (PluginCollections)base["Plugins"];
            }
        }

    }
}
