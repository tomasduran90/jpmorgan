﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections.Plugin
{
    [ConfigurationCollection(typeof(PluginElement), AddItemName = "Plugin")]
    public class PluginCollections : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new PluginElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null)
                throw new ArgumentNullException("Plugin");

            return ((PluginElement)element).Key;
        }
    }
}
