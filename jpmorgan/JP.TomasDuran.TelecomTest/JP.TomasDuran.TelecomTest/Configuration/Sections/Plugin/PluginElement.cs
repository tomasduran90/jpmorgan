﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections.Plugin
{
    public class PluginElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Key
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("type", IsRequired = true)]
        public string type
        {
            get
            {
                return (string)this["type"];
            }
            set
            {
                this["type"] = value;
            }
        }
    }

}