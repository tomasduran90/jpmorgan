﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections
{
    public class RateCalculatorSection : ConfigurationSection
    {
        [ConfigurationProperty("RateCalculator", IsRequired = false)]
        public RatesCalculatorElement RateCalculator
        {
            get
            {
                return (RatesCalculatorElement)this["RateCalculator"];
            }
            set
            {
                this["RateCalculator"] = value;
            }
        }

    }
}
