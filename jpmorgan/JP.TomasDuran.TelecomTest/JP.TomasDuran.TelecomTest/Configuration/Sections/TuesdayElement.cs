﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections
{
    public class TuesdayElement : AbstractDayElement
    {
        public TuesdayElement() : base(DayOfWeek.Tuesday) { }
    }
}
