﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections.Plan
{
    [ConfigurationCollection(typeof(PlanElement), AddItemName = "Plan")]
    public class PlanCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new PlanElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null)
                throw new ArgumentNullException("Plan");

            return ((PlanElement)element).Key;
        }
    }
}
