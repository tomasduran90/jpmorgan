﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections.Plan
{
    public class PlanElement : ConfigurationElement
    {
        [ConfigurationProperty("key", IsRequired = true)]
        public string Key
        {
            get
            {
                return (string)this["key"];
            }
            set
            {
                this["key"] = value;
            }
        }

        [ConfigurationProperty("desc", IsRequired = true)]
        public string Desc
        {
            get
            {
                return (string)this["desc"];
            }
            set
            {
                this["desc"] = value;
            }
        }

        [ConfigurationProperty("price", IsRequired = true)]
        public double Price
        {
            get
            {
                return Convert.ToDouble(this["price"]);
            }
            set
            {
                this["price"] = value;
            }
        }

        public override string ToString()
        {
            return this.Desc;
        }
    }
}
