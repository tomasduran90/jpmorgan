﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections.Plan
{
    public class PlanSection : ConfigurationSection
    {
        [ConfigurationProperty("Plans", IsDefaultCollection = true)]
        public PlanCollection PlansCollection
        {
            get
            {
                return (PlanCollection)base["Plans"];
            }
        }

    }
}
