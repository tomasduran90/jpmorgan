﻿using JP.TomasDuran.TelecomTest.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections
{
    public class AbstractDayElement : ConfigurationElement
    {
        private DayOfWeek day;
        private List<RangeValidator> validators;

        public AbstractDayElement(DayOfWeek day)
        {
            this.day = day;
            validators = new List<RangeValidator>();
        }

        [ConfigurationProperty("Ranges", IsDefaultCollection = true)]
        public RangeCollection RangeCollection
        {
            get
            {
                return (RangeCollection)base["Ranges"];
            }
        }

        public DayOfWeek Day
        {
            get
            {
                return this.day;
            }
        }

        public void AddValidator(RangeValidator val)
        {
            this.validators.Add(val);
        }

        public List<RangeValidator> Validators
        {
            get
            {
                return this.validators;
            }
        }

    }

}
