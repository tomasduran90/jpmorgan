﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Configuration.Sections
{
    public class RangeElement : ConfigurationElement
    {
        [ConfigurationProperty("plan", IsRequired = true)]
        public string Plan
        {
            get
            {
                return (string)this["plan"];
            }
            set
            {
                this["plan"] = value;
            }
        }

        [ConfigurationProperty("from", IsRequired = false)]
        public string From
        {
            get
            {
                return (string)this["from"];
            }
            set
            {
                this["from"] = value;
            }
        }

        [ConfigurationProperty("to", IsRequired = false)]
        public string To
        {
            get
            {
                return (string)this["to"];
            }
            set
            {
                this["to"] = value;
            }
        }

        [ConfigurationProperty("allDay", IsRequired = false)]
        public bool AllDay
        {
            get
            {
                return Convert.ToBoolean(this["allDay"]);
            }
            set
            {
                this["allDay"] = value;
            }
        }
    }
}
