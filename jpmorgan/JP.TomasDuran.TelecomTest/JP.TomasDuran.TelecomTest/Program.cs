﻿using JP.TomasDuran.TelecomTest.Engine;
using JP.TomasDuran.TelecomTest.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.TomasDuran.TelecomTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create Customer
            Customer customer = new Customer();
            customer.FirstName = "Tomas";
            customer.LastName = "Duran";
            customer.AlterDate = DateTime.Parse("18/01/2017 13:31:17");
            customer.Calls = new List<Call>();

            Random gen = new Random();

            int totalminutes = 0;

            Console.WriteLine("History:");

            //Load Calls To customer
            for (int i = 0; i < 20; i++)
            {
                Call call = new Call();
                DateTime start = new DateTime(1995, 1, 1);
                int range = (DateTime.Today - start).Days;
                call.DateOfCall = start.AddDays(gen.Next(range)).AddMinutes(gen.Next(1, 100000));
                call.minutesOfCall = gen.Next(1, 45);
                call.numberDestination = "1222321321";

                //Random International Calls
               /* if (i < 10)
                    call.numberDestination = "123";*/

                customer.Calls.Add(call);
                Console.WriteLine(call.DateOfCall + "   " + call.DateOfCall.DayOfWeek + "   " + call.minutesOfCall);
                totalminutes += call.minutesOfCall;
            }

            Console.WriteLine(string.Format("Total minutes: {0}", totalminutes));
            Console.WriteLine(Environment.NewLine);

            try
            {
                //Delegate Work to worker
                Worker worker = new Worker(customer);
                Task<string> result = worker.Start();
                result.Wait();
                Console.Write(result.Result);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            
            Console.ReadKey();

        }
    }
}
