﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Entities
{
    public class RangeValidator
    {
        private TimeSpan starts;
        private TimeSpan ends;
        private string plan;

        public RangeValidator(string plan, string start, string end)
        {
            this.starts = TimeSpan.Parse(start);
            this.ends = TimeSpan.Parse(end);
            this.plan = plan;
        }

        public bool isInRange(string now)
        {
            return this.isInRange(TimeSpan.Parse(now));
        }

        public bool isInRange(DateTime now)
        {
            return this.isInRange(now.TimeOfDay);
        }

        public bool isInRange(TimeSpan now)
        {
            if(this.starts <= this.ends)
            {
                if(now >= this.starts && now <= this.ends)
                {
                    return true;
                }
            }
            else
            {
                if(now >= this.starts || now <= this.ends)
                {
                    return true;
                }
            }
            return false;
        }

        public string Plan
        {
            get
            {
                return this.plan;
            }
        }
    }
}
