﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.TomasDuran.TelecomTest.Entities
{
    public class BillContextResponse
    {
        public Dictionary<string, double> plansBills { get; set;  }
        public Dictionary<string, int> plansMinutes { get; set; }
    }
}
