﻿using JP.TomasDuran.TelecomTest.Configuration;
using JP.TomasDuran.TelecomTest.Configuration.Sections.Plan;
using JP.TomasDuran.TelecomTest.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.TomasDuran.TelecomTest.Entities
{
    public abstract class LogicalPlugin
    {
        private string filterPlanName;

        public LogicalPlugin(string filterPlanName)
        {
            this.filterPlanName = filterPlanName;
        }

        public string FilterPlanName
        {
            get
            {
                return this.filterPlanName;
            }
        }

        public abstract bool ApplyFilter(Customer customer, Call call, PlanElement plan);
        
        public virtual double Calculate(PlanElement plan, int minutes)
        {
            return plan.Price * minutes;
        }


    }
}
