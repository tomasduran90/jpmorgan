﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Entities
{
    public class Call
    {
        public DateTime DateOfCall { get; set; }
        public int minutesOfCall { get; set; }
        public string numberDestination { get; set; }
    }
}
