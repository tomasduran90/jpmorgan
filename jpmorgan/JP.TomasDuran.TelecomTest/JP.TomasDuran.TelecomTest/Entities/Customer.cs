﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Entities
{
    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<Call> Calls { get; set; }
        public DateTime AlterDate { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", this.FirstName, this.LastName);
        }
    }
}
