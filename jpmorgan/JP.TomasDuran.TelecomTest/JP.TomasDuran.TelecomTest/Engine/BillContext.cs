﻿using JP.TomasDuran.TelecomTest.Configuration;
using JP.TomasDuran.TelecomTest.Configuration.Sections;
using JP.TomasDuran.TelecomTest.Configuration.Sections.Plan;
using JP.TomasDuran.TelecomTest.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JP.TomasDuran.TelecomTest.Engine
{
    public class BillContext
    {
        private BillContextResponse response;

        public BillContext()
        {
            Init();
        }

        public BillContextResponse Run(List<Call> calls)
        {
            AbstractDayElement Datefactory = null;

            foreach (Call call in calls)
            {
                Datefactory = ConfigurationInstance.Instance.GetDayWorker(call.DateOfCall.DayOfWeek);

                bool validate = false;

               

                foreach (RangeValidator validator in Datefactory.Validators)
                {
                    if (validator.isInRange(call.DateOfCall))
                    {
                        this.Add(validator.Plan, this.CalculateAmount(validator.Plan, call.minutesOfCall));
                        validate = true;
                    }
                }

                if(!validate)
                {
                    var principalPlan = Configuration.ConfigurationInstance.Instance.GetPrincipalPlan;
                    this.Add(principalPlan.Key, this.CalculateAmount(principalPlan.Key, call.minutesOfCall));
                }
            }

            return this.response;
        }

        public void Flush()
        {
            Init();
        }


        private void Add(PlanElement plan, int minutesOfCall)
        {
            if (this.response.plansMinutes.ContainsKey(plan.Key))
            {
                this.response.plansMinutes[plan.Key] += minutesOfCall;
            }
            else
            {
                this.response.plansMinutes.Add(plan.Key, minutesOfCall);
            }

            double amount = plan.Price * minutesOfCall;

            if (this.response.plansBills.ContainsKey(plan.Key))
            {
                this.response.plansBills[plan.Key] += amount;
            }
            else
            {
                this.response.plansBills.Add(plan.Key, amount);
            }
        }

        private double CalculateAmount(string plan, int minutes)
        {
            PlanElement planobj = ConfigurationInstance.Instance.GetPlanByName(plan);

            if (this.response.plansMinutes.ContainsKey(plan))
            {
                this.response.plansMinutes[plan] += minutes;
            }
            else
            {
                this.response.plansMinutes.Add(plan, minutes);
            }

            return planobj.Price * minutes;
        }

        private void Add(string plan, double callAmount)
        {
            if (this.response.plansBills.ContainsKey(plan))
            {
                this.response.plansBills[plan] += callAmount;
            }
            else
            {
                this.response.plansBills.Add(plan, callAmount);
            }
        }

        private void Init()
        {
            this.response = new BillContextResponse();
            this.response.plansBills = new Dictionary<string, double>();
            this.response.plansMinutes = new Dictionary<string, int>();
        }

    }
}
