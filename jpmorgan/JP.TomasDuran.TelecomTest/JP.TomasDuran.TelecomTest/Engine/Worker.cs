﻿using JP.TomasDuran.TelecomTest.Configuration;
using JP.TomasDuran.TelecomTest.Configuration.Sections;
using JP.TomasDuran.TelecomTest.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.TomasDuran.TelecomTest.Engine
{
    public class Worker
    {
        private BillContext context;
        private Customer customer;
        private double total;

        public Worker(Customer customer)
        {

            this.context = new BillContext();
            this.customer = customer;
            this.total = 0;
        }

        public Task<string> Start()
        {
            return Task.Run(() =>
            {
                //separate national and international calls   
                List<Call> nationalCalls = this.customer.Calls.Where(t => !isInternational(t.numberDestination)).ToList();
                List<Call> internationalCall = this.customer.Calls.Where(t => isInternational(t.numberDestination)).ToList();

                //Run bill contexts with calls, rates Engine
                BillContextResponse national = this.context.Run(nationalCalls);

                //New Customer Logic
                bool isNewUser = ApplyNewCustomerLogic(national);

                this.context.Flush();

                //Run bill contexts with calls, rates Engine
                BillContextResponse international = this.context.Run(internationalCall);

                //International Calc
                ApplyInternationalTarrif(international);

                StringBuilder response = new StringBuilder();

                response.AppendLine(string.Format("Customer: {0} New User Promotion: {1}", this.customer.ToString(), isNewUser));
                response.AppendLine("National Calls: ");
                response.AppendLine(this.RenderResponse(national));

                response.AppendLine("International Calls: ");
                response.AppendLine(this.RenderResponse(international));

                response.AppendLine(string.Format("Total $ {0}", total));

                return response.ToString();     
            });
        }


        private void ApplyInternationalTarrif(BillContextResponse response)
        {
            List<string> keys = new List<string>(response.plansBills.Keys);

            foreach (string item in keys)
            {
                response.plansBills[item] = response.plansBills[item] * 2;
            }
        }

        private bool ApplyNewCustomerLogic(BillContextResponse response)
        {
            // Regular Key is Key of plan in configFile.
            if (this.NewCustomer(this.customer) && response.plansBills.ContainsKey("Regular"))
            { 
                response.plansBills["Regular"] = response.plansMinutes["Regular"] * ConfigurationInstance.Instance.GetPlanByName("Night").Price;
                return true;
            }
            return false;
        }

        private bool NewCustomer(Customer customer)
        {
            //TODO: Configurationvalue
            return (customer.AlterDate.AddDays(365) > DateTime.Now);
        }

        private string RenderResponse(BillContextResponse response)
        {
            StringBuilder builder = new StringBuilder();

            foreach (string key in response.plansBills.Keys)
            {
                total += response.plansBills[key];
                builder.Append("Plan Name:  " + ConfigurationInstance.Instance.GetPlanByName(key).Desc + "  Minutes: " + response.plansMinutes[key]  + " Amount: " + response.plansBills[key] + Environment.NewLine);
            }
            return builder.ToString();
        }


        //Is iternational
        private bool isInternational(string number)
        {
            //val number with reg expre 54 or country code
            if (number == "123")
                return true;


            return false;
        }
    }
}
