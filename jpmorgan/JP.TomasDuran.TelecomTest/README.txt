Consigna JP MORGAN

Some rules to follow:

* Unless told otherwise please submit your response before 8:00am on the monday following your first interview so your submission can be reviewed in time.  If you are unable to do this because of other commitments please notify us beforehand.

* Do NOT upload your code to public websides such as github

* Only submit source code & any required documentation please (fileouts if using Smalltalk), no binaries or smalltalk images please as the email will be filtered and we will not be able to review your submission.

------------------------------------------------------------

A telecom company wants to implement a simple system for call billing.  

In their call plans there are different types of tariff:
- regular calls at 0.05 pence per minute
- late night calls (for example from 10pm to 4am) at 0.02 pence per minute
- weekend calls at 0.01 pence per minute

There are two kinds of clients that the company manages:
- New clients who have been given a discount on their regular calls (regular calls pay the same rate as late night calls)
- Existing clients who pay the standard rates mentioned above

In addition the company uses the following rule to charge for local & international calls:
- International calls double the rate per minute

The company is interested in implementing a simple billing system to calculate the total charge to a client given their call history.

Please use an object oriented language of your choice to allow the company to calculate the total charge to a client given their call history.

Notes:
* You DO NOT need to implement any UI for this exercise.
* You can assume that the call history data is already given to you (you do not need to write code to load it from a file, database or similar source).
* All the rules the company requires to calculate the charge for a call are given above.  There are no other rules to apply.
* You do not need to worry about determining if a call is local or international.  You can assume there is a method that can do that for you.



Como utilizar :


JP.TomasDuran.TelecomTest - Telecom company call billing System

To make a correct use of the system you must understand how to configure it. 

1 - Plans and Tarrif

For create new plans, modify existing plans and their houer range you have to open app.config file.
There you will find the planSection and the RatesCalculatorSection

- PlanSection:

 <PlanSection>
    <Plans>
      <Plan key="Regular" desc="Regular Plan" price="0.5"></Plan>
      <Plan key="Night" desc="Night Plan" price="0.1"></Plan>
      <Plan key="Weekend" desc="WeekendPlan" price="0.2"></Plan>
      <Plan key="ExtraPlan" desc="WeekendPlan" price="0.2"></Plan>
    </Plans>
 </PlanSection>

In this section you can change the price per minute of a specific plan, create new plans, or delete existing.
* The Key must be unique (if not the program will not run, exception message will be display in console).
* The desc string is the message that will be display in Bill Detail.

- RatesCalculatorSection

In this section you configure the ranges of the plans we talk about. Rates Calculator have a principal node that have configure a
specific plan for the whole set of the week. Then you have one specific Element per Day of week (It is optional, if you dont put one
day, the Rate Caluclator Plan its use it for that not configure date).
Inside each day of week you can define Collection of Range where you can play with hour range and different Types of plans.
* In Range you have to do reference to a plan with the key you define in PlanSection.
* You can define From and To with 00:00 to 23:59 for each range
* You can use allDay prop (boolean) to define all day plans
* The System controls Ranges Overlapping before running
* You can configure as many ranges as you want for one specific day referencing diferentes plans, the only condition is not to have overlapping in hours.
* Any call that is calculated outside a range hours configured it will be bill in General Rate Calculator Plan

  <RatesCalculatorSection>
    <RateCalculator plan="Regular">  <!-- General Plan -->
      <Monday>
        <Ranges>
          <Range plan="Night" from="22:00" to="04:00"></Range>  <!-- N ranges for same date, the system validate overlapping -->
 	  <Range plan="ExtraPlan" from="13:00" to="16:00"></Range>
        </Ranges>
      </Monday>
      <Tuesday>
        <Ranges>
          <Range plan="Night" from="22:00" to="04:00"></Range>
        </Ranges>
      </Tuesday>
      <Wednesday>
        <Ranges>
          <Range plan="Night" from="22:00" to="04:00"></Range>
        </Ranges>
      </Wednesday>
      <Thursday>
        <Ranges>
          <Range plan="Night" from="22:00" to="04:00"></Range>
        </Ranges>
      </Thursday>
      <Friday>
        <Ranges>
          <Range plan="Night" from="22:00" to="04:00"></Range>
        </Ranges>
      </Friday>
      <Saturday>
        <Ranges>
          <Range plan="Weekend" allDay="true"></Range>
        </Ranges>
      </Saturday>
      <Sunday>
        <Ranges>
          <Range plan="Weekend" allDay="true"></Range>
        </Ranges>
      </Sunday>
    </RateCalculator>
  </RatesCalculatorSection>